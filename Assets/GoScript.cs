﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Net;

public class GoScript : MonoBehaviour {

	private int pointerIsInCounter = 0;
	private bool pointerIsIn = false;
	private GameObject loadingBar;

	// Use this for initialization
	void Start () {
		roundStartCounter.textBeginText = "Testrunde startet in ";

		if (SceneManager.GetActiveScene().name == "EndScene")
		{
			//Upload Score to Server, removed for public Repository
		}
		loadingBar = GameObject.Find ("LoadingBar");
	}

	private int time = 0;
	// Update is called once per frame
	void Update () {
		if (pointerIsIn)
			pointerIsInCounter++;		
		if (pointerIsInCounter > 180)
			SceneManager.LoadScene("StartRoundCounter");
		time++;
		if (time / 60 > 19) {
			SceneManager.LoadScene("StartScreen_new");
		}
		GameObject restartCounter = GameObject.Find ("restartCounter");
		if (restartCounter != null)
			restartCounter.GetComponent<TextMesh> ().text = (20 - (time / 60)).ToString();

		Vector3 newScale = loadingBar.transform.localScale;
		newScale.x = (float)pointerIsInCounter / 90;
		loadingBar.transform.localScale = newScale;
	}

	public void Go() {
		pointerIsInCounter++;
		pointerIsIn = true;
		Debug.Log ("Pointer Upate: "+pointerIsIn);
	}
	public void OutOfGo() {
		pointerIsIn = false;
		pointerIsInCounter = 0;
		Debug.Log ("Pointer Upate: "+pointerIsIn);
	}
}
