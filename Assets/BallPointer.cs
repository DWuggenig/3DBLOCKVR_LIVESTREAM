﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPointer : MonoBehaviour {

	GameObject ball;
	GameObject blocker;

	private bool isActive = false;

	private int collisionCount = 0;

	private GameObject[] directionArrows;

	// Use this for initialization
	void Start () {
		ball = GameObject.Find("Ball");
		blocker = GameObject.Find("Blocker");

		directionArrows = GameObject.FindGameObjectsWithTag ("DirectionArrow");
		for (int i = 0; i < directionArrows.Length; i++) 
		{
			directionArrows [i].GetComponent<MeshRenderer> ().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Math.Abs(ball.transform.position.x - blocker.transform.position.x) >= 1.2 || Math.Abs(ball.transform.position.y - blocker.transform.position.y) >= 1.2) {
			this.gameObject.GetComponent<MeshRenderer> ().enabled = false;
		} else {
			//this.gameObject.GetComponent<MeshRenderer> ().enabled = true;
			Vector3 newPosition = new Vector3 (ball.transform.position.x, ball.transform.position.y, 41.5f);
			this.gameObject.transform.position = newPosition;

			if (isActive) {
				Vector3 scale = this.gameObject.transform.localScale;
				scale.x = 0.3f + (ball.transform.position.z / 100);
				scale.y = 0.3f + (ball.transform.position.z / 100);
				this.gameObject.transform.localScale = scale;
			} else if (ball.transform.position.z > 70)
				isActive = true;
		}

		for (int i = 0; i < directionArrows.Length; i++) 
		{
			directionArrows [i].GetComponent<MeshRenderer> ().enabled = false;
		}
		if (ball.transform.position.y - blocker.transform.position.y >= 1.2) {
			GameObject.Find ("Arrow1").GetComponentInChildren<MeshRenderer> ().enabled = true;
			if (ball.transform.position.x - blocker.transform.position.x >= 1.2)
				GameObject.Find ("Arrow6").GetComponentInChildren<MeshRenderer> ().enabled = true;
			if (ball.transform.position.x - blocker.transform.position.x <= -1.2)
				GameObject.Find ("Arrow2").GetComponentInChildren<MeshRenderer> ().enabled = true;
		} else if (ball.transform.position.y - blocker.transform.position.y <= -1.2) {
			GameObject.Find ("Arrow4").GetComponentInChildren<MeshRenderer> ().enabled = true;
			if (ball.transform.position.x - blocker.transform.position.x >= 1.2)
				GameObject.Find ("Arrow5").GetComponentInChildren<MeshRenderer> ().enabled = true;
			if (ball.transform.position.x - blocker.transform.position.x <= -1.2)
				GameObject.Find ("Arrow3").GetComponentInChildren<MeshRenderer> ().enabled = true;
		} 
		else 
		{
			if (ball.transform.position.x - blocker.transform.position.x >= 1.2) {
				GameObject.Find ("Arrow6").GetComponentInChildren<MeshRenderer> ().enabled = true;
				GameObject.Find ("Arrow5").GetComponentInChildren<MeshRenderer> ().enabled = true;
			}
			if (ball.transform.position.x - blocker.transform.position.x <= -1.2) {
				GameObject.Find ("Arrow3").GetComponentInChildren<MeshRenderer> ().enabled = true;
				GameObject.Find ("Arrow2").GetComponentInChildren<MeshRenderer> ().enabled = true;
			}
		}

	}

	void onCollisionEnter() {
		collisionCount++;
	}
	void onCollisionExit() {
		collisionCount--;
	}
}
