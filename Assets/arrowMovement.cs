﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrowMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		BlockerMovement.isTestRound = true;
	}
		
	// Update is called once per frame
	void Update () {
		Vector3 direction = GameObject.Find ("scharfstellenText").transform.position - this.gameObject.transform.position;
		this.gameObject.transform.rotation = Quaternion.LookRotation(direction);
		this.gameObject.transform.position = Camera.main.transform.forward*10;

		this.gameObject.transform.GetChild (0).GetComponent<MeshRenderer> ().enabled = true;
		if (Camera.main.transform.eulerAngles.y < 80 || Camera.main.transform.eulerAngles.y > 300)
		{
			this.gameObject.transform.GetChild (0).GetComponent<MeshRenderer> ().enabled = false;
			if (Camera.main.transform.eulerAngles.x > 30 && Camera.main.transform.eulerAngles.x < 310) 
			{
				this.gameObject.transform.GetChild (0).GetComponent<MeshRenderer> ().enabled = true;
			}
		}
	}
}
