﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBlockLightScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Light light = this.gameObject.AddComponent<Light>();
        light.color = new Color32(0x00,0x3c,0xa6,0xff);
        light.intensity = 20;
        light.transform.position = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
