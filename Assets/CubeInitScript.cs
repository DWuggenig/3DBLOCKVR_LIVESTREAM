﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeInitScript : MonoBehaviour {
    public static List<GameObject>[] lights;

    int[,] ignoreBlocks = new int[,]
    {
        {0,0},
        {0, 4},
        {5, 0},
        {5, 4}
    };

    object[,] halfBlocks = new object[0,0];
    /*{
        {1,"1:90"},
        {2, "0:90"},
        {7, "1:180"},
        {6, "0:180"},
        {0, "2:90"},
        {5, "0:180"},
        {5, "0:180"},
        {6, "1:180"},
        {7, "2:180"},

        {0, "3:0"},
        {1, "4:0"},
        {2, "5:0"},
    };*/

	int[,] dupliCubes = new int[,]
	{
		/*{1,1},
		{1,3},
		{4,1},
		{4,3},*/
	};

	int[,] superCubes = new int[,]
	{
		{0,2},
		{5,2},
	};

	int [,] smallBlockerCubes = new int[,]
	{
		{1,4},
		{4,4},
		{4,0},
		{1,0},
	};

	int [,] bigBCubes = new int[,]
	{
		{1,4},
		{4,4},
		{4,0},
		{1,0},
	};

    // Use this for initialization
    void Start () {
        CubeInitScript.lights = new List<GameObject>[10];
        for (int i = 0;i<CubeInitScript.lights.Length;i++)
        {
            CubeInitScript.lights[i] = new List<GameObject>();
        }
        for (int i = 1; i < 7; i++)
        {
            GameObject pl = GameObject.Find("Cylinder"+i);
            pl = pl.transform.parent.gameObject;
            for (int j = 0; j < 5; j++)
            {
                GameObject newLight = Instantiate(pl, pl.transform.position - new Vector3(0, 0, 12.5f * j), Quaternion.identity, pl.transform.parent);
                //newLight.transform.parent = GameObject.Find("Sidewall").transform;
                newLight.transform.localScale = pl.transform.localScale;
                newLight.transform.rotation = pl.transform.rotation;
                lights[i].Add(newLight);
            }
        }

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0;j<5;j++)
            {
                bool ignoreBlock = false;
                for (int k = 0; k < ignoreBlocks.GetLength(0); k++)
                {
                    if (ignoreBlocks[k,0] == i && ignoreBlocks[k,1] == j)
                        ignoreBlock = true;
                }

                bool halfBlock = false;
                int rotation = 0;
                for (int k = 0; k < halfBlocks.GetLength(0); k++)
                {
                    if (Convert.ToInt32(halfBlocks[k, 0]) == i && Convert.ToInt32(halfBlocks[k, 1].ToString().Split(':')[0]) == j)
                    {
                        halfBlock = true;
                        rotation = Convert.ToInt32(halfBlocks[k,1].ToString().Split(':')[1]);
                    }
                }

				bool dupliCube = false;
				for (int k = 0; k < dupliCubes.GetLength(0); k++)
				{
					if (dupliCubes[k,0] == i && dupliCubes[k,1] == j)
						dupliCube = true;
				}

				bool superCube = false;
				for (int k = 0; k < superCubes.GetLength(0); k++)
				{
					if (superCubes[k,0] == i && superCubes[k,1] == j)
						superCube = true;
				}

				bool smallBlockerCube = false;
				for (int k = 0; k < smallBlockerCubes.GetLength(0); k++)
				{
					if (smallBlockerCubes[k,0] == i && smallBlockerCubes[k,1] == j)
						smallBlockerCube = true;
				}

				bool bigBCube = false;
				for (int k = 0; k < bigBCubes.GetLength(0); k++)
				{
					if (bigBCubes[k,0] == i && bigBCubes[k,1] == j)
						bigBCube = true;
				}

                if (!ignoreBlock)
                {
                    float xOffset = -3.2f;
                    float yOffset = 0;
                    if (halfBlock)
                    {
                        GameObject halfCube = Instantiate(GameObject.Find("halfCube"), new Vector3((i * 2.8f) + xOffset, (j * 2.8f) + yOffset, (30 * (i + 1) * (j + 1)) - 3000), Quaternion.identity);
                        halfCube.transform.Rotate(0, 0, rotation);
                    }
                    else
                    {
                        GameObject cube = Instantiate(GameObject.Find("Cube"), new Vector3((i * 2f) + xOffset, (j * 2f) + yOffset, (30 * (i + 1) * (j + 1)) - 3000), Quaternion.identity);
                        cube.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
						if (dupliCube) {
							cube.name = "DupliCube";
							Material dupliCubeMaterial = Resources.Load ("DupliCubeMaterial", typeof(Material)) as Material;
							cube.GetComponent<Renderer> ().material = dupliCubeMaterial;
						} else if (superCube) {
							cube.name = "SuperCube";
							Material superCubeMaterial = Resources.Load ("SuperCubeMaterial", typeof(Material)) as Material;
							cube.GetComponent<Renderer> ().material = superCubeMaterial;
						} else if (smallBlockerCube) {
							cube.name = "SmallBlockCube";
							Material smallBlockerCubeMaterial = Resources.Load ("SmallBlockerCubeMaterial", typeof(Material)) as Material;
							cube.GetComponent<Renderer> ().material = smallBlockerCubeMaterial;
						} else if (bigBCube) 
						{
							cube.name = "BigBCube";
							Material bigBallCubeMaterial = Resources.Load("BigBallCubeMaterial", typeof(Material)) as Material;
							cube.GetComponent<Renderer> ().material = bigBallCubeMaterial;
						}
                    }
                }
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
