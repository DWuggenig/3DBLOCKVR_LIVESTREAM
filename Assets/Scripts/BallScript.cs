﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallScript : MonoBehaviour {

    public static int ballCounter = 0;
    private int bigBallCounter = 2;
    private int staySmallCounter = 2;
	private float speed = Custom.ballSpeedBeginning;
    private Rigidbody rb;

    private float cameraRotation = 0;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public int StaySmallCounter
    {
        get
        {
            return staySmallCounter;
        }

        set
        {
            staySmallCounter = value;
        }
    }

    public static int points = 0;

    public static GameObject sampleBall;

    // Use this for initialization
    void Start () {
		BallScript.points = 0;
		BallScript.ballCounter++;
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, 10);
		Physics.gravity = new Vector3(0,0,0);

		SphereCollider sc = this.gameObject.GetComponent<SphereCollider> ();
		sc.material = (PhysicMaterial) Resources.Load ("BallMaterial");

		sc.material.bounciness = Custom.bounciness;

        /*GameObject sidewalls = GameObject.Find("Sidewalls");
        sidewalls.transform.localScale -= new Vector3(0.75f, 0.75f, 0.75f);*/

    }

    // Update is called once per frame
	int time = 0;
	bool isStarted = false;
    void Update () {
		this.gameObject.GetComponent<SphereCollider>().material.bounciness = Custom.bounciness;
		time++;
		if (isStarted || GameObject.Find ("Ball").transform.position.z >= 60)
		{
			Physics.gravity = Custom.gravitation;
			isStarted = true;
		}
		if (time % 10 == 0)
		{

			if (this.gameObject.transform.position.z > -100) {
				int nLights = (int)((this.gameObject.transform.position.z) / 10) % 10;

				nLights -= 9;
				nLights *= -1;

				for (int i = 0; i < 4; i++) {
					for (int j = 1; j < 7; j++) {
						CubeInitScript.lights [j] [i].SetActive (true);
					}
				}
				for (int i = nLights; i < 5; i++) {
					for (int j = 1; j < 7; j++) {
						CubeInitScript.lights [j] [i].SetActive(false);
					}
				}
			}
		}
    }

    void OnCollisionEnter (Collision col)
    {
        if (col.gameObject.name.Contains("Cube"))
        {
            BallScript.points++;
            staySmallCounter--;
            bigBallCounter--;

            if (staySmallCounter < 1)
            {
                GameObject blocker = GameObject.Find("Blocker");
				blocker.transform.localScale = new Vector3(1, 1, 0.3f);
            }
            if (bigBallCounter < 1)
            {
                GameObject ball = GameObject.Find("Ball");
                if (ball)
                    ball.transform.localScale = new Vector3(1, 1, 1);
            }
            if (col.gameObject.name.Contains("SuperCube"))
            {
				rb.velocity = new Vector3(0, 0, -speed*Custom.speedCubeBounciness) + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
            }
            else
            {
                rb.velocity = new Vector3(0, 0, -speed) + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
            }
            col.gameObject.SetActive(false);
			if (speed < 60)
        		speed +=3f;
            Destroy(col.gameObject);
        }
        else if (col.gameObject.name.Contains("Ground"))
        {
            rb.velocity = new Vector3(0, 0, -speed) + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0);
        }
        if (col.gameObject.name.Contains("DupliCube"))
        {
            Instantiate(gameObject, new Vector3(2.7f,10,10), Quaternion.identity);
            col.gameObject.SetActive(false);
            BallScript.ballCounter++;
        }
        if (col.gameObject.name.Contains("SmallBlockCube"))
        {
            GameObject blocker = GameObject.Find("Blocker");
            //blocker.transform.localScale = new Vector3(0.75f, 0.75f, 1.125f);
			blocker.transform.localScale = new Vector3(0.75f, 0.75f, 0.3f);
            staySmallCounter = 2;
            col.gameObject.SetActive(false);
        }
        if (col.gameObject.name.Contains("DestroyBlock"))
        {
            BallScript.sampleBall = this.gameObject;
            this.gameObject.SetActive(false);
            BallScript.ballCounter--;
            if (BallScript.ballCounter == 0)
            {
                //Application.LoadLevel("EndScreen");
                BlockerMovement.startCameraRestartRotation = true;
            }
        }
        if (col.gameObject.name.Contains("BigBall"))
        {
            col.gameObject.SetActive(false);
            GameObject ball = GameObject.Find("Ball");
            ball.transform.localScale = new Vector3(2, 2, 2);
            bigBallCounter = 2;
        }
    }
}
