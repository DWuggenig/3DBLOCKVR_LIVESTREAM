﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using MKGlowSystem;
using UnityEngine.SceneManagement;

public class BlockerMovement : MonoBehaviour {

    // Use this for initialization
    private Rigidbody rb;
    private Vector3[] oldSensorPositions = new Vector3[12];

    public static bool startCameraRestartRotation = false;

    public static bool restarted = false;

    private bool firstStartCameraInit = true;

	public static bool isTestRound = true;

	private DateTime dt;

	private GameObject blockCounter;
	private GameObject timeCounter;

    void Start () {
		GameObject.Find ("LoadingSphereObject").GetComponent<Renderer> ().enabled = false;
		GameObject.Find ("LoadingSphereText").GetComponent<Renderer> ().enabled = false;
		Screen.orientation = ScreenOrientation.Landscape;

        //Add Light Script to all Cubes
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Untagged");  //returns GameObject[]
        for (int i = 0; i < gos.Length; i++)
        {
            if (gos[i].name.Contains("Cube"))
                gos[i].AddComponent<AddBlockLightScript>();
        }

        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        for (int i = 0;i<oldSensorPositions.Length;i++)
        {
			this.oldSensorPositions[i] = (Camera.main.transform.rotation * new Vector3(0.2f,0.2f,1)) * Custom.blockerSensitivity;
        }
		dt = DateTime.Now;

		blockCounter = GameObject.Find ("blockCounter");
		timeCounter = GameObject.Find ("timeCounter");
    }

	private bool skippedFirst = false;
    // Update is called once per frame
	int perfCounter = 0;
	void Update () {
		perfCounter++;
		if (perfCounter % 60 == 0) {
			blockCounter.GetComponent<TextMesh> ().text = "Blöcke: " + BallScript.points+ "/26";
			timeCounter.GetComponent<TextMesh> ().text = "Zeit: " + Math.Round (DateTime.Now.Subtract (dt).TotalSeconds);
			if (BallScript.points == 26) {
				BallScript.sampleBall = this.gameObject;
				this.gameObject.SetActive(false);
				BallScript.ballCounter--;
				if (BallScript.ballCounter == 0)
				{
					BlockerMovement.startCameraRestartRotation = true;
				}
			}
		}
        if (firstStartCameraInit)
        {
            firstStartCameraInit = false;
            GameObject.Find("Main Camera Left").AddComponent<MKGlow>();
            GameObject.Find("Main Camera Right").AddComponent<MKGlow>();
        }
        this.gameObject.SetActive(true);
		//if (BlockerMovement.startCameraRestartRotation && !BlockerMovement.restarted) 
		if (BallScript.ballCounter <= 0)
		{
			GameObject.Find ("LoadingSphereObject").GetComponent<Renderer> ().enabled = true;
			GameObject text = GameObject.Find ("LoadingSphereText");
			text.GetComponent<Renderer> ().enabled = true;
			text.transform.position = GameObject.Find("Main Camera Left").transform.position;
			text.transform.position += (Camera.main.transform.rotation * Vector3.forward) * 4;
			skippedFirst = true;
			if (skippedFirst) 
			{
				if (isTestRound) {
					roundStartCounter.textBeginText = "Echte Runde startet in ";
					SceneManager.LoadScene ("StartRoundCounter");
					isTestRound = false;
					BlockerMovement.startCameraRestartRotation = false;
				} else {
					EndScenetext.text = "Zeit\n" + Math.Round (DateTime.Now.Subtract (dt).TotalSeconds) + " Sekunden\n\nBlöcke\n" + BallScript.points;
					EndScenetext.time = (int)Math.Round (DateTime.Now.Subtract (dt).TotalSeconds);
					SceneManager.LoadScene ("EndScene");
				}
			}
		}
        if (BlockerMovement.restarted == true)
        {
            Assets.Scripts.Menu.down(new object());
        }
			
		Vector3 direction = (Camera.main.transform.rotation * Vector3.forward) * 10 * Custom.blockerSensitivity;
        direction.z = 43;
        direction.y += 4f;
        direction.x += 1.85f;


        if (Input.GetKey("w"))
        {
            direction.y = 4;
        }
        if (Input.GetKey("a"))
        {
            direction.x = -4;
        }
        if (Input.GetKey("s"))
        {
            direction.y = -4;
        }
        if (Input.GetKey("d"))
        {
            direction.x = 4;
        }


		float xBlockerMinStop = 3.8f;
		float xBlockerMaxStop = 7.3f;
		if (GameObject.Find ("Blocker").transform.localScale.x == 0.75f) {
			xBlockerMinStop = 4.5f;
			xBlockerMaxStop = 7.9f;
		}


		if (direction.x < - xBlockerMinStop + Math.Abs((direction.y-4)*0.6f))
		{
			direction.x = - xBlockerMinStop + Math.Abs((direction.y - 4) * 0.6f);
		}
		if (direction.x > xBlockerMaxStop - Math.Abs((direction.y-4)*0.6f))
		{
			direction.x = xBlockerMaxStop - Math.Abs((direction.y - 4) * 0.6f);
		}
		if (direction.y < 0)
			direction.y = 0;
		if (direction.y > 8)
			direction.y = 8;

        rb.MovePosition(direction);
    }

    private Vector3 getNewDirectionValue(Vector3 direction)
    {
        for (int i = 1;i<this.oldSensorPositions.Length;i++)
        {
            this.oldSensorPositions[i - 1] = this.oldSensorPositions[i];
        }
        this.oldSensorPositions[this.oldSensorPositions.Length - 1] = direction;

        Vector3 allSensorPositions = new Vector3(0,0,0);
        for (int i = 0;i<this.oldSensorPositions.Length;i++)
        {
            allSensorPositions += this.oldSensorPositions[i];
        }
        allSensorPositions = allSensorPositions / this.oldSensorPositions.Length;
        return allSensorPositions;
    }
}
