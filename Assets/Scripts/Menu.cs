﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    class Menu
    {
        private static string RESTART_MENU = "RESTART_MENU";
        private static float rotation = 0;

        private static float cameraOffsetZ = 0;

        private static int fontSize = 30;

        private static int round = 1;

        private static bool sentScore = false;

        internal static void up(string type)
        {
            GameObject blocker = GameObject.Find("Blocker");
            
            if (Menu.rotation > -90)
            {
                Menu.rotation -= 0.7f;
                GameObject.Find("Sidewalls").transform.RotateAround(new Vector3(1.57f, 10.84f, -15.27f), Vector3.right, -0.7f);
            }
            if (cameraOffsetZ < 5)
            {
                GameObject camera = GameObject.Find("Main Camera");
                Vector3 newPosition = camera.transform.position;
                cameraOffsetZ += 0.1f;
                newPosition.z += 0.1f;
                camera.transform.position = newPosition;
            }
            if (round < 3)
            {
                TextMesh startCounterText = GameObject.Find("startCounter").GetComponent<TextMesh>();
                startCounterText.fontSize = 30;
                startCounterText.text = Convert.ToString("RESTART");
                if (type == Menu.RESTART_MENU)
                {
                    startCounterText.text = Convert.ToString("RESTART");
                }
                if (Menu.rotation <= -90 && cameraOffsetZ >= 5)
                {
                    Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.rotation * Vector3.forward);
                    Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.rotation * Vector3.forward * 10, Color.red);

                    BoxCollider coll = GameObject.Find("startCounter").GetComponent<BoxCollider>();
                    RaycastHit hit;

                    if (coll.Raycast(ray, out hit, 100.0f))
                    {
                        if (fontSize > 100)
                        {
                            blocker.GetComponent<MeshRenderer>().enabled = true;
                            round++;
                            startCounterText.text = "Round " + round;
                            fontSize = 30;
                            StartCounter.timeElapsed = 0;
                            BlockerMovement.restarted = true;
                            BlockerMovement.startCameraRestartRotation = false;
                        }
                        fontSize++;
                        startCounterText.fontSize = fontSize;
                    }
                    else
                    {
                        if (fontSize > 30)
                            fontSize--;
                        startCounterText.fontSize = fontSize;
                    }
                }
            }
            else
            {
                TextMesh startCounterText = GameObject.Find("startCounter").GetComponent<TextMesh>();
                startCounterText.fontSize = 50;
                startCounterText.text = Convert.ToString("END!\nPOINTS: " + BallScript.points);
                if (!sentScore)
                {
                    sentScore = true;
                    /*WebClient wc = new WebClient();
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    wc.DownloadString("http://challenge.anexia-it.com/script/admin/3DBLOCKVR/savePoints.php?points=" + BallScript.points);*/
                }
            }

            
        }
        internal static void down(object type)
        {
            StartCounter.firstStart = false;
            GameObject blocker = GameObject.Find("Blocker");
            if (blocker)
                blocker.GetComponent<MeshRenderer>().enabled = true;
            if (Menu.rotation < 0)
            {
                Menu.rotation += 0.7f;
                GameObject.Find("Sidewalls").transform.RotateAround(new Vector3(1.57f, 10.84f, -15.27f), Vector3.right, 0.7f);
            }
            else
            {
                GameObject ball = BallScript.sampleBall;
                ball.transform.position = new Vector3(2, 10, 3);
                ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                ball.SetActive(true);
                BallScript.ballCounter++;
                BlockerMovement.restarted = false;
            }
            if (cameraOffsetZ > 0)
            {
                GameObject camera = GameObject.Find("Main Camera");
                Vector3 newPosition = camera.transform.position;
                cameraOffsetZ -= 0.1f;
                newPosition.z -= 0.1f;
                camera.transform.position = newPosition;
            }
        }
    }
}
