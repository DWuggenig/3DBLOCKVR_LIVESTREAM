﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCounter : MonoBehaviour {

    public static int timeElapsed;
    float rotation = 0;
    float offsetY = 0;
    float offsetZ = 0;

    float cameraOffsetZ = 0;

    private TextMesh startCounterText;

    private GameObject sidewalls;
    private GameObject camera;
    private GameObject blocker;

    public static int startCounter = 5;

    public static bool firstStart = true;

    // Use this for initialization
    void Start () {
        this.startCounterText = GameObject.Find("startCounter").GetComponent<TextMesh>();
        //this.startCounterText.alignment = TextAlignment.Center;
        this.startCounterText.anchor = TextAnchor.MiddleCenter;

        sidewalls = GameObject.Find("Sidewalls");
        camera = GameObject.Find("Main Camera");
        blocker = GameObject.Find("Blocker");
    }
	
	// Update is called once per frame
	void Update () {
        timeElapsed++;
        int timeTilStart = StartCounter.startCounter - (int)Math.Round((double)timeElapsed / 60);
        if (timeTilStart >= 1 && firstStart)
        {
            this.startCounterText.text = Convert.ToString(timeTilStart);
            blocker.GetComponent<MeshRenderer>().enabled = false;
        }
        

        if (timeTilStart<1)
        {
            this.startCounterText.text = "Round 1";
            startCounterText.fontSize = 30;

            if (this.rotation < 90)
            {
                this.rotation += 0.7f;
                sidewalls.transform.RotateAround(new Vector3(1.57f,10.84f,-15.27f),Vector3.right, 0.7f);
            }
            if (this.offsetY > -0.7f && this.rotation > 89)
            {
                blocker.GetComponent<MeshRenderer>().enabled = true;
                this.offsetY -= 0.1f;
                Vector3 newPosition = sidewalls.transform.position;
                newPosition.y -= 0.1f;
                sidewalls.transform.position = newPosition;
            }
            if (this.offsetY <= -0.7f && this.rotation > 89 && cameraOffsetZ >= -7)
            {
                Vector3 newPosition = camera.transform.position;
                cameraOffsetZ -= 0.1f;
                newPosition.z -= 0.1f;
                camera.transform.position = newPosition;
            }
            if (this.offsetY <= -0.7f && this.rotation > 89 && cameraOffsetZ <= -7)
            {
                GameObject go = GameObject.Find("Ball");
                if (go)
                {
                    this.startCounterText.text = Convert.ToString("");
                    Rigidbody rb = go.GetComponent<Rigidbody>();
                    rb.isKinematic = false;
                }
            }
        }
    }
}
