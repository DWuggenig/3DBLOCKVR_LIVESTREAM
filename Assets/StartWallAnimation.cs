﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWallAnimation : MonoBehaviour {

    private int offsetZ = 0;
	private int speed = 10;
    private bool inverse = false;
    private bool animationFinished = false;


    // Use this for initialization
    void Start () {
        if (this.gameObject.name == "Ground")
        {
            offsetZ = 108;
        }
        else if (this.gameObject.name.Contains("Cube"))
        {
            inverse = true;
            offsetZ = 95;
        }
        if (this.gameObject.name.Contains("Blocker"))
        {
            offsetZ = -25;
        }
        if (this.gameObject.name.Contains("Ball"))
        {
            offsetZ = 90;
            inverse = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (!animationFinished) {
			Vector3 currentPosition = this.gameObject.transform.position;
			if (!inverse) {
				if (currentPosition.z > offsetZ) {
					if (currentPosition.z - 20 < offsetZ) {
						currentPosition.z = offsetZ;
						animationFinished = true;
					} else
						currentPosition.z -= speed;
					this.gameObject.transform.position = currentPosition;
				}
			} else {
				if (currentPosition.z < offsetZ) {
					if (currentPosition.z + 20 > offsetZ) {
						currentPosition.z = offsetZ;
						animationFinished = true;
					} else
						currentPosition.z += speed;
					this.gameObject.transform.position = currentPosition;
				}
			}
		}
	}
}
