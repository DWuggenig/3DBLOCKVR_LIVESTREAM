﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCounterScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find ("StartCounter").GetComponent<TextMesh> ().text = "";
	}

	private int time = 0;
	// Update is called once per frame
	void Update () {
		time++;

		if (time / 60 > 6) 
		{
			GameObject.Find ("StartCounter").GetComponent<TextMesh> ().text = "";
		}
		else if (time / 60 > 5) 
		{
			GameObject.Find ("StartCounter").GetComponent<TextMesh> ().text = "1";
		}
		else if (time/60>4) 
		{
			GameObject.Find ("StartCounter").GetComponent<TextMesh> ().text = "2";
		}
		else if (time/60>3) 
		{
			GameObject.Find ("StartCounter").GetComponent<TextMesh> ().text = "3";
		}
	}
}
