﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Net;
using System.Net.Sockets;

public class EndScriptRestartCounter : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}

	int time;
	// Update is called once per frame
	void Update () {
		time++;
		int timeLeft = 10 - (time / 60);
		if (timeLeft < 1)
			SceneManager.LoadScene("StartScreen_new");
		GameObject.Find("restartCounter").GetComponent<TextMesh>().text = timeLeft.ToString();
	}
}