﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCubeActiveScript : MonoBehaviour {

	private int time = 0;

	// Use this for initialization
	void Start () {
		this.gameObject.GetComponent<BoxCollider> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		this.time++;
		if (time > 700) 
		{
			this.gameObject.GetComponent<BoxCollider> ().enabled = true;
		}
	}
}
