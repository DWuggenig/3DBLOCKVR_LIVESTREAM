﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class roundStartCounter : MonoBehaviour {

	public static string textBeginText = "Testrunde startet in ";

	// Use this for initialization
	void Start () {
		
	}

	int time = 0;
	// Update is called once per frame
	void Update () {
		time++;

		int timeLeft = 5-time / 60;
		if (timeLeft < 1)
			SceneManager.LoadScene("3DBLOCKVR_new");
		this.GetComponent<TextMesh> ().text = roundStartCounter.textBeginText+timeLeft.ToString();
	}
}
